var apiUrl = process.env.API_URL
if (!apiUrl) {
  apiUrl = 'localhost:5000'
}

module.exports = {
  chainWebpack: config => {
    config
      .plugin('define')
      .tap(args => {
        args[0] = {
          ...args[0],
          'VUE_APP_BACKEND_URL': apiUrl
          // other stuff
        }
        return args
      })
  }
}
