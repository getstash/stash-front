import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/dashboard/Dashboard.vue'
import CreateBoard from './views/createBoard/CreateBoard.vue'
import Board from './views/board/Board.vue'
import PublicBoard from './views/publicBoard/publicBoard.vue'
import Login from './views/login/Login.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/board/create',
      name: 'createBoard',
      component: CreateBoard
    },
    {
      path: '/board/:id/edit',
      name: 'editBoard',
      component: CreateBoard
    },
    {
      path: '/board/:id',
      name: 'board',
      component: Board
    },
    {
      path: '/public/board/:id',
      name: 'publicBoard',
      component: PublicBoard
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})
