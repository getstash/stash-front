import { ErrTypeNotAuthorized, ErrTypeNotExpected } from '../client/client'
import { SET_ERROR_MUTATION } from '../constants'

export function processApiErr (err, router, commit) {
  if (err === null) {
    return
  }

  if (err.type === ErrTypeNotAuthorized) {
    router.push({ name: 'login' })
  }

  if (err.type === ErrTypeNotExpected) {
    commit(SET_ERROR_MUTATION, null, { root: true })
  }
}
