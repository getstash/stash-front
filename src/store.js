import Vue from 'vue'
import Vuex from 'vuex'
import DashboardModule from './views/dashboard/module'
import CreateBoardModule from './views/createBoard/module'
import BoardModule from './views/board/module'
import PublicBoardModule from './views/publicBoard/module'
import LoginModule from './views/login/module'
import { SET_ERROR_MUTATION } from './constants'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isErrorOccured: false
  },
  mutations: {
    [SET_ERROR_MUTATION] (state) {
      state.isErrorOccured = true
    }
  },
  actions: {

  },
  modules: {
    'dashboard': DashboardModule,
    'createBoard': CreateBoardModule,
    'board': BoardModule,
    'publicBoard': PublicBoardModule,
    'login': LoginModule
  }
})
