import SwaggerClient from 'swagger-client'
import spec from './spec.json'

export const ErrTypeNotAuthorized = 1
export const ErrTypeNotFound = 2
export const ErrTypeNotExpected = 3

class ApiClient {
  async getClient (username, password) {
    let basicAuth = { username: username, password: password }
    let specWithChanges = spec
    // Update host from webpack build environment
    specWithChanges['host'] = process.env.VUE_APP_BACKEND_URL

    let client = await SwaggerClient({
      spec: spec,
      authorizations: { 'basicAuth': basicAuth },
      enableCookies: true
    })
    client.http.withCredentials = true
    return client
  }

  prepareResponse (err, data) {
    let errInfo = {
      'type': null,
      'err': err
    }
    if (err != null) {
      switch (err.status) {
        case 401:
          errInfo.type = ErrTypeNotAuthorized
          break
        case 404:
          errInfo.type = ErrTypeNotFound
          break
        default:
          errInfo.type = ErrTypeNotExpected
          break
      }
    }
    return {
      'err': errInfo,
      'data': data
    }
  }

  processError (err) {
    let errInfo = {
      'type': null,
      'err': err
    }

    switch (err.status) {
      case 401:
        errInfo.type = ErrTypeNotAuthorized
        break
      case 404:
        errInfo.type = ErrTypeNotFound
        break
      default:
        errInfo.type = ErrTypeNotExpected
        break
    }

    return errInfo
  }

  async callApi (func, params) {
    try {
      let result = await func(params)
      return [result, null]
    } catch (err) {
      return [null, this.processError(err)]
    }
  }

  async getBoardById (boardId) {
    let client = await this.getClient()

    let [data, err] = await this.callApi(client.apis.Board.get_board__boardId_, { boardId: boardId })
    if (err !== null) {
      return [null, err]
    }

    return [data.body, null]
  }

  async getBoards () {
    let client = await this.getClient()

    let [data, err] = await this.callApi(client.apis.Board.get_board, {})
    if (err !== null) {
      return [null, err]
    }

    return [data.body.boards, null]
  }

  async createBoard (name, linksIds, isOpen) {
    let client = await this.getClient()

    let boardInfo = {
      name: name,
      linkIds: linksIds,
      isOpen: isOpen
    }

    let [data, err] = await this.callApi(client.apis.Board.post_board, { board: boardInfo })
    if (err !== null) {
      return [null, err]
    }

    return [data, null]
  }

  async updateBoard (boardID, linksIds, name, isOpen) {
    let client = await this.getClient()

    let boardInfo = {
      name: name,
      linkIds: linksIds,
      isOpen: isOpen
    }

    let [data, err] = await this.callApi(client.apis.Board.post_board__boardId_, { board: boardInfo, boardId: boardID })
    if (err !== null) {
      return [null, err]
    }

    return [data.body, null]
  }

  async deleteBoard (boardId) {
    let client = await this.getClient()

    let [data, err] = await this.callApi(client.apis.Board.delete_board__boardId_, { boardId: boardId })
    if (err !== null) {
      return [null, err]
    }

    return [data, null]
  }

  async getLinks (limit, offset, term) {
    let client = await this.getClient()

    let [data, err] = await this.callApi(client.apis.Link.get_link, { limit: limit, offset: offset, q: term })
    if (err !== null) {
      return [null, err]
    }

    return [data.body.links, null]
  }

  async login (username, password) {
    let client = await this.getClient(username, password)
    await this.callApi(client.apis.Login.get_login, {})
  }

  async logout () {
    let client = await this.getClient()
    await this.callApi(client.apis.Login.post_logout, {})
  }
}

const client = new ApiClient()
// TODO: REMOVE ME
window.globalClient = client

export default client
