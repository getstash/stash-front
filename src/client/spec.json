{
  "swagger": "2.0",
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "info": {
    "title": "A Links sharing application",
    "version": "0.2.0"
  },
  "basePath": "/api/v1",
  "schemes": [
    "http"
  ],
  "securityDefinitions": {
    "basicAuth": {
      "type": "basic"
    }
  },
  "definitions": {
    "Link": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "readOnly": true
        },
        "name": {
          "type": "string"
        },
        "url": {
          "type": "string"
        },
        "imageUrl": {
          "type": "string"
        },
        "excerpt": {
          "type": "string"
        },
        "externalId": {
          "type": "string"
        },
        "source": {
          "type": "integer",
          "format": "int64"
        },
        "added": {
          "type": "string",
          "format": "date-time"
        },
        "tags": {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      }
    },
    "NewLink": {
      "type": "object",
      "required": [
        "name",
        "url"
      ],
      "properties": {
        "name": {
          "type": "string"
        },
        "url": {
          "type": "string"
        },
        "tags": {
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      }
    },
    "Board": {
      "type": "object",
      "required": [
        "name",
        "id",
        "linksCount",
        "isOpen"
      ],
      "properties": {
        "linksCount": {
          "type": "integer",
          "format": "int64"
        },
        "isOpen": {
          "type": "boolean"
        },
        "name": {
          "type": "string"
        },
        "id": {
          "type": "string"
        }
      }
    },
    "NewBoard": {
      "type": "object",
      "required": [
        "name",
        "linkIds"
      ],
      "properties": {
        "linkIds": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "name": {
          "type": "string"
        },
        "isOpen": {
          "type": "boolean"
        }
      }
    },
    "BoardWithLinks": {
      "type": "object",
      "required": [
        "name",
        "links",
        "id"
      ],
      "properties": {
        "name": {
          "type": "string"
        },
        "links": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Link"
          }
        },
        "id": {
          "type": "string"
        },
        "isOpen": {
          "type": "boolean"
        }
      }
    }
  },
  "responses": {
    "UnauthorizedError": {
      "description": "Authentication information is missing or invalid",
      "headers": {
        "WWW_Authenticate": {
          "type": "string"
        }
      }
    },
    "InternalError": {
      "description": "Internal server error"
    }
  },
  "paths": {
    "/link": {
      "get": {
        "parameters": [
          {
            "name": "q",
            "in": "query",
            "description": "search query",
            "required": false,
            "type": "string"
          },
          {
            "name": "offset",
            "in": "query",
            "description": "offset",
            "required": false,
            "type": "integer",
            "format": "int32"
          },
          {
            "name": "limit",
            "in": "query",
            "description": "offset",
            "required": false,
            "type": "integer",
            "format": "int32"
          }
        ],
        "tags": [
          "Link"
        ],
        "responses": {
          "200": {
            "description": "list available links",
            "schema": {
              "type": "object",
              "properties": {
                "links": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/Link"
                  }
                },
                "offset": {
                  "type": "integer",
                  "format": "int32"
                },
                "limit": {
                  "type": "integer",
                  "format": "int32"
                }
              }
            }
          },
          "401": {
            "$ref": "#/responses/UnauthorizedError"
          },
          "500": {
            "$ref": "#/responses/InternalError"
          }
        }
      },
      "post": {
        "description": "Creates a new link. Duplicates are allowed",
        "tags": [
          "Link"
        ],
        "parameters": [
          {
            "name": "links",
            "in": "body",
            "description": "Links to be added",
            "required": true,
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/NewLink"
              }
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Links are created"
          },
          "400": {
            "description": "Error in link description"
          },
          "401": {
            "$ref": "#/responses/UnauthorizedError"
          },
          "500": {
            "$ref": "#/responses/InternalError"
          }
        }
      }
    },
    "/board": {
      "post": {
        "description": "Creates a new board.",
        "tags": [
          "Board"
        ],
        "parameters": [
          {
            "name": "board",
            "in": "body",
            "description": "new board to be created",
            "required": true,
            "schema": {
              "$ref": "#/definitions/NewBoard"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "board created"
          },
          "401": {
            "$ref": "#/responses/UnauthorizedError"
          },
          "500": {
            "$ref": "#/responses/InternalError"
          }
        }
      },
      "get": {
        "tags": [
          "Board"
        ],
        "responses": {
          "200": {
            "description": "list available links",
            "schema": {
              "type": "object",
              "properties": {
                "boards": {
                  "type": "array",
                  "items": {
                    "$ref": "#/definitions/Board"
                  }
                }
              }
            }
          },
          "401": {
            "$ref": "#/responses/UnauthorizedError"
          },
          "500": {
            "$ref": "#/responses/InternalError"
          }
        }
      }
    },
    "/board/{boardId}": {
      "get": {
        "tags": [
          "Board"
        ],
        "parameters": [
          {
            "name": "boardId",
            "in": "path",
            "required": true,
            "description": "The id of the board to retrieve",
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "get board",
            "schema": {
              "$ref": "#/definitions/BoardWithLinks"
            }
          },
          "401": {
            "$ref": "#/responses/UnauthorizedError"
          },
          "404": {
            "description": "no board"
          },
          "500": {
            "$ref": "#/responses/InternalError"
          }
        }
      },
      "post": {
        "tags": [
          "Board"
        ],
        "parameters": [
          {
            "name": "boardId",
            "in": "path",
            "required": true,
            "description": "The id of the board that will be updated",
            "type": "string"
          },
          {
            "name": "board",
            "in": "body",
            "description": "updates to board",
            "required": true,
            "schema": {
              "$ref": "#/definitions/NewBoard"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "get board",
            "schema": {
              "$ref": "#/definitions/BoardWithLinks"
            }
          },
          "401": {
            "$ref": "#/responses/UnauthorizedError"
          },
          "404": {
            "description": "no board"
          },
          "500": {
            "$ref": "#/responses/InternalError"
          }
        }
      },
      "delete": {
        "tags": [
          "Board"
        ],
        "parameters": [
          {
            "name": "boardId",
            "in": "path",
            "required": true,
            "description": "The id of the board to delete",
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "board deleted"
          },
          "401": {
            "$ref": "#/responses/UnauthorizedError"
          },
          "404": {
            "description": "no board"
          },
          "500": {
            "$ref": "#/responses/InternalError"
          }
        }
      }
    },
    "/login": {
      "get": {
        "tags": [
          "Login"
        ],
        "security": [
          {
            "basicAuth": []
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "401": {
            "$ref": "#/responses/UnauthorizedError"
          },
          "500": {
            "$ref": "#/responses/InternalError"
          }
        }
      }
    },
    "/logout": {
      "post": {
        "tags": [
          "Login"
        ],
        "responses": {
          "200": {
            "description": "Success"
          }
        }
      }
    }
  }
}
