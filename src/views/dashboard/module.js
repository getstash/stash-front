import client from '../../client/client'
import router from '../../router'
import { processApiErr } from '../../common/utils'

export const SET_LOADING_MUTATION = 'SET_LOADING_MUTATION'
export const REMOVE_LOADING_MUTATION = 'REMOVE_LOADING_MUTATION'
export const UPDATE_BOARDS_MUTATION = 'UPDATE_BOARDS_MUTATION'

export const FETCH_BOARDS_ACTION = 'FETCH_BOARDS_ACTION'
export const DELETE_BOARD_ACTION = 'DELETE_BOARD_ACTION'

async function fetchBoardsOrReturnErr (commit) {
  const [boards, err] = await client.getBoards()
  if (err !== null) {
    return err
  }
  commit(UPDATE_BOARDS_MUTATION, boards)
  return null
}

const DashboardModule = {
  namespaced: true,
  state: {
    boards: [],
    isLoading: false
  },
  mutations: {
    [SET_LOADING_MUTATION] (state) {
      state.isLoading = true
    },
    [REMOVE_LOADING_MUTATION] (state) {
      state.isLoading = false
    },
    [UPDATE_BOARDS_MUTATION] (state, newBoards) {
      state.boards = newBoards
    }
  },
  actions: {
    async [FETCH_BOARDS_ACTION] ({ commit }) {
      commit(SET_LOADING_MUTATION)

      const err = await fetchBoardsOrReturnErr(commit)

      commit(REMOVE_LOADING_MUTATION)

      processApiErr(err, router, commit)
    },
    async [DELETE_BOARD_ACTION] ({ commit }, boardId) {
      commit(SET_LOADING_MUTATION)
      commit(UPDATE_BOARDS_MUTATION, [])

      await client.deleteBoard(boardId)
      const err = await fetchBoardsOrReturnErr(commit)

      commit(REMOVE_LOADING_MUTATION)

      processApiErr(err, router, commit)
    }
  }
}

export default DashboardModule
