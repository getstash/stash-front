import client from '../../client/client'
import { processApiErr } from '../../common/utils'
import router from '../../router'

export const SET_BOARD_MUTATION = 'SET_BOARD_MUTATION'
export const SET_LOADING_MUTATION = 'SET_LOADING_MUTATION'
export const REMOVE_LOADING_MUTATION = 'REMOVE_LOADING_MUTATION'

export const FETCH_BOARD_ACTION = 'FETCH_BOARD_ACTION'
export const CLEAN_BOARD_ACTION = 'CLEAN_BOARD_ACTION'

const PublicBoardModule = {
  namespaced: true,
  state: {
    isLoading: false,
    board: {
      id: '',
      links: [],
      name: '',
      isOpen: false
    }
  },
  mutations: {
    [SET_LOADING_MUTATION] (state) {
      state.isLoading = true
    },
    [REMOVE_LOADING_MUTATION] (state) {
      state.isLoading = false
    },
    [SET_BOARD_MUTATION] (state, board) {
      state.board = board
    }
  },
  actions: {
    async [FETCH_BOARD_ACTION] ({ commit }, boardId) {
      commit(SET_LOADING_MUTATION)
      let [result, err] = await client.getBoardById(boardId)
      if (err === null) {
        commit(SET_BOARD_MUTATION, result)
      }
      commit(REMOVE_LOADING_MUTATION)

      processApiErr(err, router, commit)
    },
    [CLEAN_BOARD_ACTION] ({ commit }) {
      let emptyBoard = {
        id: '',
        links: [],
        name: '',
        isOpen: false
      }
      commit(SET_BOARD_MUTATION, emptyBoard)
    }
  }
}

export default PublicBoardModule
