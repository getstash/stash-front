import client from '../../client/client'
import { processApiErr } from '../../common/utils'
import router from '../../router'

export const SET_LOADING_MUTATION = 'SET_LOADING_MUTATION'
export const REMOVE_LOADING_MUTATION = 'REMOVE_LOADING_MUTATION'
export const ADD_LINKS_MUTATION = 'ADD_LINKS_MUTATION'
export const ADD_LINK_TO_BOARD_MUTATION = 'ADD_LINK_TO_BOARD_MUTATION'
export const REMOVE_LINK_FROM_BOARD_MUTATION = 'REMOVE_LINK_FROM_BOARD_MUTATION'
export const CHANGE_BOARD_OPENNES_MUTATION = 'CHANGE_BOARD_OPENNES_MUTATION'
export const SET_BOARD_NAME_MUTATION = 'SET_BOARD_NAME_MUTATION'
export const SET_BOARD_ID_MUTATION = 'SET_BOARD_ID_MUTATION'
export const CHANGE_TERM_MUTATION = 'CHANGE_TERM_MUTATION'
export const REMOVE_LOADED_LINKS_MUTATION = 'REMOVE_LOADED_LINKS_MUTATION'
export const REMOVE_SELECTED_LINKS_MUTATION = 'REMOVE_SELECTED_LINKS_MUTATION'

export const FETCH_LINKS_ACTION = 'FETCH_LINKS_ACTION'
export const CHANGE_TERM_ACTION = 'CHANGE_TERM_ACTION'
export const ADD_LINK_TO_BOARD_ACTION = 'ADD_LINK_TO_BOARD_ACTION'
export const REMOVE_LINK_FROM_BOARD_ACTION = 'REMOVE_LINK_FROM_BOARD_ACTION'
export const CLEAN_PAGE_ACTION = 'CLEAN_PAGE_ACTION'
export const CREATE_BOARD_ACTION = 'CREATE_BOARD_ACTION'
export const UPDATE_BOARD_ACTION = 'UPDATE_BOARD_ACTION'
export const FETCH_BOARD_ACTION = 'FETCH_BOARD_ACTION'

const FETCH_LIMIT = 25

function defaultBoardName () {
  return 'AnonymousBoard: ' + Date.now()
}

const CreateBoardModule = {
  namespaced: true,
  state: {
    linksSection: {
      links: [],
      searchTerm: '',
      linksLoaded: 0
    },
    board: {
      id: '',
      links: [],
      name: '',
      isOpen: false
    },
    isLoading: false
  },
  mutations: {
    [SET_LOADING_MUTATION] (state) {
      state.isLoading = true
    },
    [REMOVE_LOADING_MUTATION] (state) {
      state.isLoading = false
    },
    [CHANGE_TERM_MUTATION] (state, newSearchTerm) {
      state.linksSection.searchTerm = newSearchTerm
    },
    [REMOVE_LOADED_LINKS_MUTATION] (state) {
      state.linksSection.links = []
      state.linksSection.linksLoaded = 0
    },
    [REMOVE_SELECTED_LINKS_MUTATION] (state) {
      state.board.links = []
    },
    [CHANGE_BOARD_OPENNES_MUTATION] (state, opennes) {
      state.board.isOpen = opennes
    },
    [SET_BOARD_NAME_MUTATION] (state, boardName) {
      state.board.name = boardName
    },
    [SET_BOARD_ID_MUTATION] (state, boardId) {
      state.board.id = boardId
    },
    [ADD_LINK_TO_BOARD_MUTATION] (state, link) {
      state.board.links = [
        ...state.board.links,
        link
      ]

      let filteredLinks = state.linksSection.links.filter((item) => {
        if (item.id === link.id) {
          return false
        }
        return true
      })

      state.linksSection.links = filteredLinks
    },
    [REMOVE_LINK_FROM_BOARD_MUTATION] (state, link) {
      let filteredLinks = state.board.links.filter((item) => {
        if (item.id === link.id) {
          return false
        }
        return true
      })

      state.board.links = filteredLinks

      state.linksSection.links = [
        ...state.linksSection.links,
        link
      ]
    },
    [ADD_LINKS_MUTATION] (state, newLinks) {
      // Filter links that already in board
      let existingLinkIdsMap = state.board.links.reduce((obj, value) => {
        if (value == null) {
          return obj
        }
        obj[value.id] = true
        return obj
      }, {})

      let filteredLinks = newLinks.filter((link) => {
        if (existingLinkIdsMap[link.id]) {
          return false
        }
        return true
      })

      state.linksSection.links = [
        ...state.linksSection.links,
        ...filteredLinks
      ]
      state.linksSection.linksLoaded += newLinks.length
    }
  },
  actions: {
    async [FETCH_LINKS_ACTION] ({ commit, state }) {
      commit(SET_LOADING_MUTATION)

      let [links, err] = await client.getLinks(FETCH_LIMIT, state.linksSection.linksLoaded, state.linksSection.searchTerm)

      if (err === null) {
        commit(ADD_LINKS_MUTATION, links)
      }
      commit(REMOVE_LOADING_MUTATION)

      processApiErr(err, router, commit)
    },
    [CHANGE_TERM_ACTION] ({ commit, state }, newTerm) {
      commit(CHANGE_TERM_MUTATION, newTerm)
      commit(REMOVE_LOADED_LINKS_MUTATION)
    },
    [ADD_LINK_TO_BOARD_ACTION] ({ commit, state }, link) {
      commit(ADD_LINK_TO_BOARD_MUTATION, link)
    },
    [REMOVE_LINK_FROM_BOARD_ACTION] ({ commit, state }, link) {
      commit(REMOVE_LINK_FROM_BOARD_MUTATION, link)
    },
    [CLEAN_PAGE_ACTION] ({ commit }) {
      commit(CHANGE_TERM_MUTATION, '')
      commit(REMOVE_LOADED_LINKS_MUTATION)
      commit(REMOVE_SELECTED_LINKS_MUTATION)
    },
    async [CREATE_BOARD_ACTION] ({ state, commit }, { boardName, isOpen }) {
      let linkIds = state.board.links.map((link) => {
        return link.id
      })

      if (boardName === '') {
        boardName = defaultBoardName()
      }

      let [, err] = await client.createBoard(boardName, linkIds, isOpen)

      processApiErr(err, router, commit)
    },
    async [UPDATE_BOARD_ACTION] ({ state, commit }, { boardName, isOpen }) {
      commit(SET_LOADING_MUTATION)

      let linkIds = state.board.links.map((link) => {
        return link.id
      })

      let [, err] = await client.updateBoard(state.board.id, linkIds, boardName, isOpen)
      commit(REMOVE_LOADING_MUTATION)

      processApiErr(err, router, commit)
    },
    async [FETCH_BOARD_ACTION] ({ state, commit }, boardId) {
      commit(SET_LOADING_MUTATION)

      let [result, err] = await client.getBoardById(boardId)
      if (err === null) {
        for (let link of result.links) {
          commit(ADD_LINK_TO_BOARD_MUTATION, link)
        }
        commit(SET_BOARD_NAME_MUTATION, result.name)
        commit(CHANGE_BOARD_OPENNES_MUTATION, result.isOpen)
        commit(SET_BOARD_ID_MUTATION, result.id)
      }
      commit(REMOVE_LOADING_MUTATION)

      processApiErr(err, router, commit)
    }
  }
}

export default CreateBoardModule
