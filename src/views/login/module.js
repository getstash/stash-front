import client from '../../client/client'

export const SET_SHOW_ERROR_MUTATION = 'SET_SHOW_ERROR_MUTATION'
export const DELETE_SHOW_ERROR_MUTATION = 'DELETE_SHOW_ERROR_MUTATION'

export const LOGIN_ACTION = 'LOGIN_ACTION'

const LoginModule = {
  namespaced: true,
  state: {
    showErrorMessage: false
  },
  mutations: {
    [SET_SHOW_ERROR_MUTATION] (state) {
      state.showErrorMessage = true
    },
    [DELETE_SHOW_ERROR_MUTATION] (state) {
      state.showErrorMessage = false
    }
  },
  actions: {
    async [LOGIN_ACTION] ({ commit }, data) {
      await client.login(data.username, data.password)
    }
  }
}

export default LoginModule
