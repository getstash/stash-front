import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import infiniteScroll from 'vue-infinite-scroll'
import { VueMasonryPlugin } from 'vue-masonry'

Vue.config.productionTip = false
Vue.use(infiniteScroll)
Vue.use(VueMasonryPlugin)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
