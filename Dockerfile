FROM node:8.12.0-alpine

MAINTAINER Alexei Stoliarov <alexei.stoliarov@gmail.com>

RUN npm install -g @vue/cli

RUN mkdir /code
COPY ./package.json /code
WORKDIR /code
RUN npm install

# Move code to the last stage to not reinstall app dependencies at every change in repo
ADD . /code
# docker-build.sh will be used to copy build result into directory that mounted to nginx
RUN chmod +x ./docker-build.sh

